---
layout: post
current: post
cover:  assets/svg/jacked_and_hacked.svg
navigation: True
title: Jacked and Hacked
date: 2021-06-01 19:17:00
tag: writing
class: post-template
subclass: 'post'
author: derrick
excerpt: A short story in the SF genre.
---
[Jacked and Hacked (.pdf)](/data/Jacked_And_Hacked.pdf){:target="_blank"}

## Outline
Young couple, Brianna and Tyler, newly married, decide to give each other the gift of being jacked for pleasure.
They sign up for ExtaJax, the new pseudo-medicinal pleasure stimulating treatment based on room temperature SQUID devices and transcranial stmulation.
Happy times ensue.
Brianna finds out that the interface can be hacked to monitor the pleasure signal.
This can be used in a positive feedback loop to increase normal pleasure.
Happier times ensue.
Brianna next hacks a cross-feedback loop – the pleasure of the partner triggers one's own pleasure.
Even happier times ensue.
Then they get the bright idea to crank up the gain.
Turns sour because of an unintended consequence that being near each other causes waves of manic depression.
Decide to pull the plug. But the damage is done.
They cannot be physically near each other any longer without spiraling cyclic debilitating depression.
In the end Brianna creates a work-around.
