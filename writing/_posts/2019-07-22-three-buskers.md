---
layout: post
current: post
cover:  assets/art/IMG_20221220_085548.jpg
navigation: True
title: The Three Buskers
date: 2019-07-22 19:13:05
tag: writing
class: post-template
subclass: 'post'
author: derrick
excerpt: An attempt at a fable.
---
[The Three Buskers (.pdf)](/data/The_Three_Buskers.pdf){:target="_blank"}

Just a simple story to see if I could write a fable.
