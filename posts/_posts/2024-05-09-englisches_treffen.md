---
layout: post
current: post
cover:  assets/images/english.jpg
navigation: True
title: Englisches Treffen
date: 2024-05-09 08:47:08
tags: [retirement]
class: post-template
subclass: 'post'
author: derrick
comments_host: c.im
comments_user_id: derrickoswald
comments_id: 112410566011897674
excerpt: I lead an „English Conversation“ meetup group every second Wednesday morning.
---

I lead an „[English Conversation](https://www.baertschihus.ch/app/download/10419253584/Do+you+speak+English+ab+Februar+2024.pdf?t=1715072476)“ meetup group every second Wednesday morning.
It’s one aspect of the [Seniorencafé](https://www.baertschihus.ch/bistrot/seniorencaf%C3%A9/) (seniors café) at the Bärtschihus in Gümligen, that also includes activities like Jass (a card game like Euchre), lectures, eating, etc.

Initially the format was just me putting out a few conversation starters, like “What did you do for your vacation?” and “Describe the view from your balcony or window.”.
Now we use a textbook to put it on a more formal footing.
I would have never suspected that my high school grammar would be put to such a test.

Most urban Swiss know English better than I know German.
Some participants have lived in an English country or worked at an English speaking firm at one time or another, so for them it’s essentially an effort in recall and practice.
Others &mdash; who learned English in school just like I didn’t learn French in school &mdash; want to get more fluent.
Ages range from 68 to 86, and the attendance ranges from 8 to 12 people each meetup.

The feedback I’ve received is pretty positive.
The people keep showing up, so there must be some benefit.
[Scientific studies](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8633567/) of second language training in senior participants tend to suggest that learning a second language improves your brain.
From my own experience, operating in a second language is exhausting; so from a "use it or lose it" perspective it has to be good for you.
On the other hand, when searching for a word, sometimes now the German word is retrieved first, suggesting a second language replaces some primary language in working memory &mdash; meaning there's really only room for one language.

The sessions are sometimes lively and filled with laughter.
To me this is the real benefit.
In my opinion, anything that makes you laugh stimulates a dopamine response; like eating, sex and playing games, which people will tend to seek out.
So, maybe they do it just because it's fun.


Image credit: [Wannapik Studio](https://www.wannapik.com)