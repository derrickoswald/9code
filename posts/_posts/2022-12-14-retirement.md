---
layout: post
current: post
cover:  assets/images/zytglogge.jpg
navigation: True
title: Retirement Fantasies
date: 2022-12-14 13:17:57
tag: retirement
class: post-template
subclass: 'post'
author: derrick
excerpt: Of all the possible scenarios for retirement that I entertain, the most persistent is the 'city artist'.
---

Of all the possible scenarios for retirement that I entertain, the most persistent is the 'city artist'.

What could be more pleasant than spending an afternoon in a quiet corner of the 'Altstadt' of Bern or Fribourg sketching or painting.
Even if one was not inclined to do art, it would still be a very nice way to pass the time in a picturesque location.

Before actually trying it out, I see it going down like this:

I set up a tiny portable three-legged stool and an easel in the shadow of a building, facing an interesting streetscape or building façade.
Fortunately, I've had the foresight to capture the scene with a camera beforehand, and using an overhead projector have transferred the basic outline
to an A3 sheet in pencil... so I'm not starting from a completely blank page.

I whip out a small watercolour paintbox or a set of fountain pens of various sizes, and start transferring the real scene to the page.
I embellish the process with large swoops of arm and significant pauses to step back and look at the work in formation,
holding up my thumb at arms length or my hands shaped as a viewfinder as artists are wont to do.

A little group of tourists pass by and stop to watch, providing some choice words of encouragement,
but mostly with comments of a jealous nature - "Oh, I wish I could do that".
A conversation might ensue about the relative merits of various art techniques or materials,
which delays the art performance, but adds to the spectacle.

As the crowd dissipates and I finish up the final strokes, a child walks by - a chubby hand holding the mother's finger - and asks "what is that man doing".
With some explanation, she gets the gist of it across, 
and I offer to give them the painting/sketch as I sign it - because the problem isn't making art, it's getting rid of it when you're done.
After some words are exchanged, they agree to take it, if I provide a rubber-band to roll it up with.

I pack up my kit and head home for dinner as the westering sun catches the top of the buildings.