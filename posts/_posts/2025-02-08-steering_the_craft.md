---
layout: post
current: post
cover:  assets/images/steering.jpg
navigation: True
title: "Steering the Craft"
date: 2025-02-08 17:27:45
tags: [writing]
class: post-template
subclass: 'post'
author: derrick
comments_host: c.im
comments_user_id: derrickoswald
comments_id: 113969778494817976
excerpt: If anyone is attempting the exercises in Ursula K. Le Guin's Steering the Craft, I'm open to exchanging critiques.
---
If anyone is attempting the exercises in Ursula K. Le Guin's Steering the Craft, I'm open to exchanging critiques.
I haven't finished the book, and there's really no reason to expect that I will, but maybe somebody is in the same boat.
I couldn't find anything in social media that pointed me to a like minded group, so I'll have to start my own.

[Workbook: Steering the Craft: A Twenty-First-Century Guide to Sailing the Sea of Story](/data/Steering%20the%20Craft%20-%20Workbook.pdf)
