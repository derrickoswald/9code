---
layout: post
current: post
cover: assets/images/ideas.jpg
navigation: True
title: Welcome to 9code
date: 2022-12-04 09:00:00
tag: meta
class: post-template
subclass: 'post'
author: derrick
excerpt: It's been a long time in gestation, but I'm finally rolling around to updating the web site.
---

Hi. It's been a long time in gestation, but I'm finally rolling around to updating the web site.

So far, their isn't much content, but that will change as I dive into retirement in 2023.

### Concept

As with any startup, there are a number of options. This site will hopefully be the intersection of three areas:
what the reader is interested in, what I know a little about, and what can fit in a web site.

![three way Venn Diagram: interesting; I know; fits the web](/assets/svg/concept.svg)

A non-exhaustive list will include stuff like:

- current topical subjects
- philosophical pieces
- technical posts
- art
- writing
- mundane day-to-day junk

Primarily, this is a way to hone my art and writing skills by regularly publishing articles, paintings, short stories, comics and so on. In the future, I can perhaps combine my long-hand nightly journalling and sketching with this site, but for now they are separate.

What I want you, the reader, to take away from each item is maybe a little additional knowledge about me or a subject, some amount of happiness -- if only saying "I'm glad that's not me", and a feeling of community in the world of the future. 

In the end, this may be the legacy that the friends and family I leave behind can relate to. 