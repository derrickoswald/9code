---
layout: post
current: post
cover:  assets/images/ranch.jpg
navigation: True
title: Ranch Dressing
date: 2023-11-10 17:50:20
tags: [rant, food]
class: post-template
subclass: 'post'
author: derrick
excerpt: One of the things you can't get in Switzerland is ranch salad dressing.
---

One of the things you can't get in Switzerland is ranch salad dressing.
There are many types of French and Italian dressings, a Catalina and a Caesar, but not ranch.

Admittedly, it's very American. Being a Canadian is almost like being an American though.
So, I kind of grew up with it in the 80's when it became available from Kraft (now Mondelez) in bottled form.

I've gathered all (I hope) the ingredients, and will set about trying to recreate
what I remember was a creamy, herby, topping that would turn a salad 
(or spicy chicken wings) into an experience.

- 150ml buttermilk, well shaken
- 100ml sour cream
- 50ml mayonaise
- 6ml lemon juice
- 3ml dry mustard
- 3ml onion power
- 3ml garlic powder
- 2ml black pepper, fresh ground
- 2ml Tobasco/Worcestershire sauce
- 1ml salt
- 18ml dill, finely chopped
- 18ml parsley, finely chopped
- 18ml chives, finely chopped

Whisk together buttermilk, sour cream, mayonnaise, lemon juice, mustard, onion, garlic, pepper, hot pepper sauce and salt. Stir in dill, parsley and chives.
