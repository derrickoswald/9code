---
layout: post
current: post
cover:  assets/images/wiring_rearrangement_thumbnail.jpg
navigation: True
title: Renovation
date: 2023-01-05 13:16:11
tag: retirement
class: post-template
subclass: 'post'
author: derrick
excerpt: And so it begins. Samantha's Le Carro project is well underway, but I just started after retirement.
---

And so it begins.
Samantha's Le Carro project is well underway,
but I just started after retirement.
There is a lot I can help with,
of which the most pressing was electric wire rearrangement.

The idea was to move the cables running through the attic down off the beam (upper right) which will be exposed,
and instead run them along the floor, which will be built up to make a usable space for sleeping quarters,
after adding about 30cm of insulation to the walls and ceiling.

![Le Carro wiring rearrangement](/assets/images/wiring_rearrangement.jpg)

Most of the task was determining which wires are live and need to be preserved,
and which can be removed.
The extra loops of wire showing will be tucked away, or maybe I'll trim them if they're in the way.
Sadly, I didn't take a before picture; only an after picture.
Maybe that's a good thing, since that would show how little I actually did.

At the moment, the restaurant portion of the property has electricity and is heated,
but not my assigned sleeping quarters in the residential part.
This leads to a "camping" flavour, with a sleeping bag and battery lamp.
Hopefully this stage will be over soon come spring.
