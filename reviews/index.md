---
layout: page
current: reviews
title: Reviews
navigation: true
logo: 'assets/images/newyears.jpg'
class: page-template
subclass: 'post page'
---

These are yearly wrap-ups as seen at the time. No attempt has been made to correct them, so there may be anachronisms and inconsistencies.
They do, however, give an overview of my thinking at the time.

- [2024 The Year In Review](/data/2024 The Year In Review.pdf){:target="_blank"}
- [2023 The Year In Review](/data/2023 The Year In Review.pdf){:target="_blank"}
- [2022 The Year In Review](/data/2022 The Year In Review.pdf){:target="_blank"}
- [2021 The Year In Review](/data/2021 The Year In Review.pdf){:target="_blank"}
- [2020 The Year In Review](/data/2020 The Year In Review.pdf){:target="_blank"}
- [2019 The Year In Review](/data/2019 The Year In Review.pdf){:target="_blank"}
- [2018 The Year In Review](/data/2018 The Year In Review.pdf){:target="_blank"}
- [2017 The Year In Review](/data/2017 The Year In Review.pdf){:target="_blank"}
- [2016 The Year In Review](/data/2016 The Year In Review.pdf){:target="_blank"}
- [2015 The Year In Review](/data/2015 The Year In Review.pdf){:target="_blank"}
- [2014 The Year In Review](/data/2014 The Year In Review.pdf){:target="_blank"}
- [2013 The Year In Review](/data/2013 The Year In Review.pdf){:target="_blank"}
- [2012 The Year In Review](/data/2012 The Year In Review.pdf){:target="_blank"}
- [2011 The Year In Review](/data/2011 The Year In Review.pdf){:target="_blank"}
- [2010 The Year In Review](/data/2010 The Year In Review.pdf){:target="_blank"}
- [2009 The Year In Review](/data/2009 The Year In Review.pdf){:target="_blank"}

Some other old notes that may be amusing or enlightening.

- [A Short History Since High School](/data/A Short History Since High School.pdf){:target="_blank"}
- [Some Differences Between Switzerland & Canada](/data/Some Differences Between Switzerland & Canada.pdf){:target="_blank"}
- [The Things I Miss](/data/The Things I Miss.pdf){:target="_blank"}
- [History of Nexsys Consulting Inc.](/data/History of Nexsys Consulting Inc.pdf){:target="_blank"}