---
layout: page
current: about
title: About
navigation: true
cover: assets/images/blog-cover.jpg
class: page-template
subclass: 'post page'
---

This blog is just a personal endeavor. It used to be the website of our company **9code GmbH**[^1] which no longer exists.

### Concept

As with any startup, there are a number of options. This site will hopefully be the intersection of three areas:
what the reader is interested in, what I know a little about, and what can fit in a web site.

![three way Venn Diagram: interesting; I know; fits the web](/assets/svg/concept.svg)

A non-exhaustive list will include stuff like:

- current topical subjects
- philosophical pieces
- technical posts
- art
- writing
- mundane day-to-day junk

Primarily, this is a way to hone my art and writing skills by regularly publishing articles, paintings, short stories, comics and so on. In the future, I can perhaps combine my long-hand nightly journalling and sketching with this site, but for now they are separate.

What I want you, the reader, to take away from each item is maybe a little additional knowledge about me or a subject, some amount of happiness -- if only saying "I'm glad that's not me", and a feeling of community in the world of the future. 

In the end, this may be the legacy that the friends and family I leave behind can relate to.

If you want to see the details behind the sausage-factory, the site is on [Gitlab](https://gitlab.com/derrickoswald/9code){:target="_blank"}.

[^1]: 9code GmbH was a company using scalable open source software for electric distribution network intelligence. Registergericht: Handelsregister des Kantons Bern Registernummer: CHE-445.811.043
