---
layout: page
current: souvenir
title: Souvenir Instructions
navigation: true
cover: assets/images/blog-cover.jpg
class: page-template
subclass: 'post page'
---

The souvenir page is a map of our photographs that are spatially registered via the [geolocation tags in the EXIF meta-data](https://en.wikipedia.org/wiki/Exif#Geolocation){:target="_blank"}
 of [jpg](https://en.wikipedia.org/wiki/JPEG){:target="_blank"} images.

Initially, it is a heat-map. Zooming in enough will expose black dots for individual photos. Clicking on a dot will show a thumbnail and image metadata.

Clicking through the thumbnail images to the full resolution images requires a login id and password, which I will set up and give to you if you ask.

[Go To The Map](/souvenir){:target="_blank"}

To use the map, you need to know the following basic operations:

### Navigation

- **Zoom** *click + or - in upper right hand corner* of the map, or on mobile *two finger pinch*, or on desktop use the *mouse wheel*, or *shift-left-click and hold, drag window, release*
- **Pan** on mobile *one fnger drag* or on desktop *left-click and hold, drag, release*
- **Rotate** on mobile *two finger twist* or on desktop *control-left-click and hold, drag left or right, release*
  (reset North to vertical by *clicking compass icon in upper right hand corner*)
- **3D** only on desktop *control-left-click and hold, drag up or down, release*

### Controls

The controls toolbox in the upper right hand corner has the following features:

- <img src="https://cdn.jsdelivr.net/gh/derrickoswald/CIMSpace@master/css/font/src/mapboxgl-ctrl-zoom-in.svg" alt="Zoom in" width="18px" height="18px" style="display: inline-block; margin: 0"> to zoom in
- <img src="https://cdn.jsdelivr.net/gh/derrickoswald/CIMSpace@master/css/font/src/mapboxgl-ctrl-zoom-out.svg" alt="Zoom out" width="18px" height="18px" style="display: inline-block; margin: 0"> to zoom out
- <img src="https://cdn.jsdelivr.net/gh/derrickoswald/CIMSpace@master/css/font/src/mapboxgl-ctrl-compass.svg" alt="Reset map" width="18px" height="18px" style="display: inline-block; margin: 0"> to reset the orientation to North upward in 2D
- <img src="https://cdn.jsdelivr.net/gh/derrickoswald/CIMSpace@master/css/font/src/zoome.svg" alt="Zoom extents" width="18px" height="18px" style="display: inline-block; margin: 0"> to zoom to the extents of the map


## WebGL

The map uses [Mapbox GL](https://www.mapbox.com/mapbox-gl-js/api/){:target="_blank"} and requires WebGL to be enabled in your browser.
All current browsers have WebGL support, but WebGL support is dependent on GPU support and may not be available
on older devices/browsers, due to a lack of up-to-date video drivers.

You can test if your browser supports WebGL by navigating to [https://get.webgl.org/](https://get.webgl.org/){:target="_blank"} to see if a spinning cube is displayed.

If not, for some older browser versions, WebGL can be enabled as follows: 

**FireFox**

Alter the browser default settings in the configuration

- Browse page about:config
- Set webgl.force-enabled to true

**Chrome**

Modify the startup command to include the parameter: --ignore-gpu-blacklist

- Find and right click on the Chrome shortcut you use (normally on the desktop or in the start menu) .
- Select 'Properties' or 'Eigenschaften'
- Add the '--ignore-gpu-blacklist' flag without the quotes at the end of the 'Target' or 'Zeil' box:
- For me the command is thus: "C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --ignore-gpu-blacklist 
