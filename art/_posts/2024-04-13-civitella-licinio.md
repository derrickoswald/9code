---
layout: post
current: post
cover: assets/art/IMG_20240413_090206_thumbnail.jpg
navigation: True
title: Via di Civitella Licinio
date: 2024-04-13 09:48:26
tags: [art, retirement, civitella]
class: post-template
subclass: 'post'
author: derrick
excerpt: A good friend of mine invited me down to his family home in Italy, where I got to do some au plein air painting.
---

A good friend of mine invited me down to his family home in Italy, where I got to do some au plein air painting.
This is one of a few streets leading to the square his house is on.

The idea was to write letters on the back of artwork, kind of like postcards,
but I found out that it's very difficult to send mail from his town.
The post office is only open two days a week, and it doesn't sell stamps!
I eventually posted the letters from Naples before we caught the train home.

| Data | Description |
| ----------- | ----------- |
| Title | Via di Civitella Licinio |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on fine grain, acid free paper |
| Dimensions (w &times; h cm)| 21 &times; 29.7 |
| Date | 2024 |

![watercolour and ink of a street in Civitella Licinio](/assets/art/IMG_20240413_090206.jpg)

[a photograph of the street](/assets/art/IMG_20240413_114826.jpg)