---
layout: post
current: post
cover: assets/art/IMG_20240414_110627_thumbnail.jpg
navigation: True
title: Chiesa di San Bartolomeo Apostolo
date: 2024-04-14 09:06:27
tags: [art, retirement, civitella]
class: post-template
subclass: 'post'
author: derrick
excerpt: As in Switzerland, the churches in Italy are equiped with bells, which they inflict on nearby people at a regular cadence.
---

As in Switzerland, the churches in Italy are equipped with bells,
which they inflict on nearby people at a regular cadence.
This church has augmented its main weapon with a couple of cute little bells that are not really deadly but just annoying.

The locals were kind enough to say I captured the essence of it,
but looking back on it now, I think I failed rather miserably.

| Data | Description |
| ----------- | ----------- |
| Title | Chiesa di San Bartolomeo Apostolo |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on fine grain, acid free paper |
| Dimensions (w &times; h cm)| 21 &times; 29.7 |
| Date | 2024 |

![watercolour and ink of a church in Civitella Licinio](/assets/art/IMG_20240414_110627.jpg)

[a photograph of the church](/assets/art/IMG_20240413_175145.jpg)