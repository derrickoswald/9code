---
layout: post
current: post
cover: assets/art/IMG_20230905_183556_thumbnail.jpg
navigation: True
title: Münsterplattform
date: 2023-09-05 16:35:56
tags: [art, retirement, stadtkünstler, USk]
class: post-template
subclass: 'post'
author: derrick
excerpt: I fell down a rabbit hole. There exists an Urban Sketchers Switzerland, and the Zurich sketch event this weekend is part of its yearly symposium. Angst level 11.
---

I fell down a rabbit hole.
There exists an [Urban Sketchers Switzerland](https://switzerland.urbansketchers.org/),
and the [Zurich sketch event this weekend](https://switzerland.urbansketchers.org/2023/08/samstag-9-september-sketchcrawl-usk.html) is part of its [yearly symposium](https://symposium.usk-switzerland.ch/de/).
Angst level 11.

I thought I would join the [Urban Sketchers Bern](https://www.facebook.com/groups/227965074572113/) on Saturday at Sechseläutenplatz to check out the group.
Moving from the on-line world to In Real Life (IRL) is always a bit of a crap shoot.
But, instead of a cozy dozen or so people from Bern, it's two hundred people from all over the world &mdash; who actually paid to do art.

So, I'm practicing again: for speed. I got the time down to two hours forty-five minutes; so there's still some way to go to hit two hours.
Although, to be fair, I talked to a guy for fifteeen minutes.

He suggested leaving it as a pencil sketch &mdash;
probably because I showed him my two previous sad attempts.
But, I watercoloured it anyway, even though not doing it would save a half hour.

Even in this smaller sketchbook format, it's easy to get lost doing detailed pencil,
so don't look too closely at the area below the nave-tower junction.
To my credit, in the interest of time, I didn't erase it and re-do it.

I still lack that loose, natural feel you see in good art from a real artist,
but it does have a few paint spots à la [@gretared](https://vis.social/@gretared) style.

| Data | Description |
| ----------- | ----------- |
| Title | Münsterplattform |
| Artist | Derrick Oswald | 
| Medium | Watercolour and pencil on acid-free paper |
| Dimensions (w &times; h cm)| 14.8 &times; 21 |
| Date | 2023 |

![watercolour and pencil of the Münsterplattform in Bern - looking north-west towards the Münster](/assets/art/IMG_20230905_183556.jpg)

