---
layout: post
current: post
cover: assets/art/IMG_20240621_103332_thumbnail.jpg
navigation: True
title: Schützenbrunnen
date: 2024-06-21 08:33:32
tags: [art, fountain]
class: post-template
subclass: 'post'
author: derrick
comments_host: c.im
comments_user_id: derrickoswald
comments_id: 112653949218158669
excerpt: You can't spit in Bern without hitting a fountain (Brunnen). This fountain is on the main street (Marktgasse) just west of the Zytglogge, and hence it is a major tourist photo op.
---
You can't spit in Bern without hitting a fountain (Brunnen). This fountain is on the main street (Marktgasse) just west of the Zytglogge, and hence it is a major tourist photo op.
I've painted many fountains &mdash; being one of the major themes in my brief painting career.
The major advantage is they stand very still and can be captured at leisure.
I have a bucket list item to paint all the fountains of Bern, but there's still lots of time.

This painting has been on the back-burner for a while &mdash; like from August last year &mdash; 
but other things get in the way and priorities shift; you know.
I'm pleased with the focal point - where things with more importance are darker and accentuated,
but it could be even better if the fountain was given higher contrast.
The background actually took the longest time, since it has so much detail relative to the fountain.
I tried to make the 'cloud effect' where the edges are omitted and only suggestions of background
are portrayed, but alas, the skill still escapes me.

There are many other failed attempts &mdash; like portraying the gold leaf of the fountain and the
pink sandstone gargoyles, and weathered basement doors, etc. Don't judge without trying it yourself.

| Data | Description |
| ----------- | ----------- |
| Title | Schützenbrunnen |
| Artist | Derrick Oswald |
| Medium | Watercolour on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2024 |

![watercolour of Schützenbrunnen](/assets/art/IMG_20240621_103332.jpg)

[original photograph of the Schützenbrunnen](/assets/art/Schützenbrunnen.jpg)