---
layout: post
current: post
cover: assets/art/IMG_20221220_085548_thumbnail.jpg
navigation: True
title: Pont de Basalio
date: 2019-07-22 19:03:00
tags: [art, landscape]
class: post-template
subclass: 'post'
author: derrick
excerpt: I have it in my head that I can create illustrated stories. This picture goes with "The Three Buskers" fable.
---
I have it in my head that I can create illustrated stories. This picture goes with [The Three Buskers](/data/The_Three_Buskers.pdf){:target="_blank"} fable.

My early crude style is ink and watercolour, which is supposed to emulate the stories and illustrations of Beatrix Potter,
but obviously fails badly. Normally I use nibs and ink, but this was done with fine-liner.

| Data | Description |
| ----------- | ----------- |
| Title | Pont de Basalio |
| Artist | Derrick Oswald |
| Medium | Watercolour and fine-liner on cartridge paper |
| Dimensions (w &times; h cm)| 41 &times; 29.7 |
| Date | 22.7.2019 |

![watercolor of three mendicants crossing a stone bridge into a walled town](/assets/art/IMG_20221220_085548.jpg)