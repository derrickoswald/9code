---
layout: post
current: post
cover: assets/art/IMG_20230903_201050_thumbnail.jpg
navigation: True
title: Urban Sketching
date: 2023-09-03 18:10:50
tags: [art, retirement, stadtkünstler, USk]
class: post-template
subclass: 'post'
author: derrick
excerpt: I joined the Urban Sketchers Bern Facebook group - which fills me full of angst because there are real artists involved.
---
I joined the [Urban Sketchers Bern](https://www.facebook.com/groups/227965074572113/) Facebook group &mdash;
which fills me full of angst because there are real artists involved.
So, I thought I would try the protocol and just sit and sketch to see how bad it will be.

The first painting is from where we sit waiting at the Zytglogge tram stop.
That's not the 9 tram and 10 bus stop north of Zytglogge at Kornhausplatz;
nor the 12 bus stop east of Zytglogge on Kramgasse;
but the 6, 7, and 8 tram stop south of Zytglogge at Casinoplatz.
Pity the fool who has instructions to meet at Zytglogge.

It is my usual ink and watercolour &mdash; but without cheating!
Just sit down and draw.
It also uses fineliner (magic marker) instead of a nib dip pen &mdash; I can't see bringing that out of the studio.
The trouble is, it looks like a child drew it.
I limited myself to the two hours the Urban Sketchers allot,
and that hurry-up style leads to gross simplifications and elided parts.
Basically, I spent too long on the pencil layout.
By the time I got to inking, the sun was up over the Casino and I was blinded.

I used a limited palette &mdash; a cute little 10cm&times;12cm metal box purchased from the Paul Klee Center gift shop.
After all, you can never have too many art supplies.
But this means I have to work out new mixtures for the pale green sandstone and terracotta clay roof tile colours from the ten provided.
I obviously haven't nailed it quite yet.

| Data | Description |
| ----------- | ----------- |
| Title | Casinoplatz |
| Artist | Derrick Oswald | 
| Medium | Watercolour and fineliner on acid-free paper |
| Dimensions (w &times; h cm)| 21 &times; 14.8 |
| Date | 2023 |

![watercolour and fineliner of the Casinoplatz in Bern - showing the Casino and Burgerbibliotek](/assets/art/IMG_20230903_201007.jpg)

To save time, for the second painting I thought I would go direct from pencil, skipping the inking step.
It was a bit challenging perspective-wise, so it actually took longer because it involved a lot of erasing and redrawing.

My debriefing notes for this one say "Capture the shadows once &mdash; they change".
Over the three hours this took, the shadows moved around like a kid playing 'monsters' with a flashlight under the chin.
I added shadows with pencil, rather than trying to add them with grey at watercolour time,
but this leads to a confused mess of diagonal hatching that obscure the cornices.
I'll get better eventually.

| Data | Description |
| ----------- | ----------- |
| Title | Brunngasse |
| Artist | Derrick Oswald | 
| Medium | Watercolour and pencil on acid-free paper |
| Dimensions (w &times; h cm)| 21 &times; 14.8 |
| Date | 2023 |

![watercolour and pencil sketch of Brunngasse, Bern, looking north in morning sun illumination](/assets/art/IMG_20230903_201050.jpg)
