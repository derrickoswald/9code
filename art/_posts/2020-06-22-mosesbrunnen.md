---
layout: post
current: post
cover: assets/art/IMG_20221220_085111_thumbnail.jpg
navigation: True
title: Mosesbrunnen
date: 2019-06-22 13:15:00
tags: [art, fountain]
class: post-template
subclass: 'post'
author: derrick
excerpt: Overlooking the Berner Münsterplatz, Mosesbrunnen dates from 1544 and provides a glimps into the heavily Christian nature of Bern.
---
Overlooking the Berner Münsterplatz, Mosesbrunnen dates from 1544 and provides a glimps into the heavily Christian nature of Bern.

This fountain is really colourful, but sadly, I made the mistake of drawing this with my daily driver fountain pen instead of waterproof ink, so it never got painted. Those astute residents of Bern will note that he's facing the wrong way with the Münster flanking behind, but as an artist one is allowed some license
when portraying things (like removing cranes and overhead lights, or turning things to advantage).

| Data | Description |
| ----------- | ----------- |
| Title | Mosesbrunnen |
| Artist | Derrick Oswald |
| Medium | Ink on cartridge paper |
| Dimensions (w &times; h cm)| 29.7 &times; 41 |
| Date | 22.06.2020 |

![watercolor of mosesbrunnen](/assets/art/IMG_20221220_085111.jpg)