---
layout: post
current: post
cover: assets/art/IMG_20230823_154121_thumbnail.jpg
navigation: True
title: Vasileios
date: 2023-08-23 13:41:21
tags: [art, portrait]
class: post-template
subclass: 'post'
author: derrick
excerpt: This is another in the series of portraits of colleagues - those poor guinea pigs of my retirement art hobby.
---
This is another in the series of portraits of colleagues - those poor guinea pigs of my retirement art hobby.

Generating these is getting a little easier, because it is more formulaic:

- note that these steps follow the "[draw the rest of the fucking owl](https://knowyourmeme.com/memes/how-to-draw-an-owl)" meme and I'm sorry, but that's how it is
- wash the lightest flesh-tone over the entire face ears and neck except for the eyeballs
- overlay red, violet, brown etc. to match the complexion and facial contours
- add eye details until it looks like real eyes
- add eyebrows
- add ear details until it looks like a real ear
- carefully add lip color until it looks like real lips
- add facial hair - obviously not required for female portraits
- add head hair
- round out with neck and clothing approximations
- stop - you'll just make it worse since watercolor is unforgiving

Of importance is to accentuate the features that are distinctive for the face,
in a kind-of caricature, but without too much exaggeration.
In this Vasileios case, it is the upper lip pointed "V" shape,
eyebrows and dominant mustache.
I don't know if anyone would recognize him if he was clean-shaven.

Color match is difficult.
The original photograph was taken in shaded summer light,
which gave him a more ruddy complexion than usual.
So, I mostly ignored that,
and instead went with my gut feeling of an olive skin base,
with sun reddened highlights.
It might have worked,
but it could be a bit more vibrant with a little more courage on my part.

He asked, "With or without glasses?" and I said with,
because he is rarely seen without them.
This presents a slight challenge to get the shape correct in perspective.
and I have to admit, I could have done a bit better.
It is what it is.

| Data | Description |
| ----------- | ----------- |
| Title | Vasileios |
| Artist | Derrick Oswald | 
| Medium | Watercolour on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2023 |

![watercolour of Vasileios](/assets/art/IMG_20230823_154121.jpg)

[original photograph of Vasileios](/assets/art/Vasileios.jpg)