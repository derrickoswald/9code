---
layout: post
current: post
cover: assets/art/IMG_20230309_095342_thumbnail.jpg
navigation: True
title: Heiliggeistkirche
date: 2023-03-09 08:03:28
tags: [art, landscape]
class: post-template
subclass: 'post'
author: derrick
excerpt: This is my second attempt at the Heiliggeistkirche.
---
This is my second attempt at the [Heiliggeistkirche](https://en.wikipedia.org/wiki/Church_of_the_Holy_Ghost,_Bern).
The [first](/assets/art/IMG_20230314_162205.jpg) was not so successful.

I was gifted a voucher for Klubschule Migros, which I've applied to an Aquarelle course with Franziska Ewald.
Much of the results from the weekly lessons are "experimental" and not worth posting.
This time I came prepared with an ink sketch and managed to complete it in the two allotted hours.

The view is from our #6 tram platform at the Bern train station.
Most times the hour, weather, and fog conspire to hide it, but I got a good shot last week when the sun came out.

For the quintessential yellow/green colour,
that's typical for Bern buildings constructed from sandstone quarried from the Ostermundigenberg,
Franziska suggested Lemon Yellow + Cerulean Blue + Quinacridone Violet,
rather than my first choice of Cadmium Yellow + Ultramarine Blue + Burnt Umber.
I think it worked well.

Some places in the painting are a little confused, since I had to "shop-out"
the rummies and vagabonds that gather on the steps and hang out on the benches around the church.

| Data | Description |
| ----------- | ----------- |
| Title | Heiliggeistkircke |
| Artist | Derrick Oswald |
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2023 |

![watercolour of Heiliggeistkirche, Bern](/assets/art/IMG_20230309_095342.jpg)

