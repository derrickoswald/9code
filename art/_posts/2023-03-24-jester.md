---
layout: post
current: post
cover: assets/art/IMG_20230324_154628_thumbnail.jpg
navigation: True
title: Jester
date: 2023-03-24 14:53:55
tags: [art, abstract]
class: post-template
subclass: 'post'
author: derrick
excerpt: This was my last painting for the watercolour course. A little frivolous work derived from a playing card joker.
---
This was my last painting for the watercolour course. A little frivolous work derived from a playing card joker.

It is what they call a "tube painting" because the paints are used directly from the tube without combining different colours.
There's some decision to be made between using hatching in the ink and shades of grey for the shadows.
I used both, but perhaps should have left out the hatching.

It is still what I would call artless. If I made another thousand, while learning from my mistakes, I might get good at it.

| Data | Description |
| ----------- | ----------- |
| Title | Jester |
| Artist | Derrick Oswald |
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2023 |

![watercolour of a jester](/assets/art/IMG_20230324_154628.jpg)

