---
layout: post
current: post
cover: assets/art/IMG_20221220_085501_thumbnail.jpg
navigation: True
title: Bern by Train
date: 2018-11-12 20:20:00
tags: [art, landscape]
class: post-template
subclass: 'post'
author: derrick
excerpt: We're train people, owning no car. One of the stellar views out of train windows is this view coming into Bern by train over the Lorraineviadukt.
---
We're train people, owning no car. There are a number of stellar views from train windows; Lake Geneva (lac Léman) coming into Lausanne after exiting the long tunnel, the Alps in general,
and this view coming into Bern by train over the Lorraineviadukt.

My sad rendition of the Berner Münster, also reflected in the Aare river and backed by the Alps,
fails completely to capture the subtle grey and blues of the distant mountains
as viewed from the Berner Oberland, which are hidden by fog during the winter
but come creeping back on little cat feet in the spring, to jump out at you unexpectedly.
The picture also doesn't capture the feeling of returning home after a long commute,
which is probably a feeling common to everyone who sees the place they know and call home from the quintessential viewpoint.

| Data | Description |
| ----------- | ----------- |
| Title | Bern by Train |
| Artist | Derrick Oswald |
| Medium | Watercolour and fine-liner on cartridge paper |
| Dimensions (w &times; h cm)| 41 &times; 29.7 |
| Date | 2018 |

![watercolor of Bern, Switzerland from the viewpoint of a train crossing the Lorraineviadukt](/assets/art/IMG_20221220_085501.jpg)
