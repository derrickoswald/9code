---
layout: post
current: post
cover: assets/images/silvias_thumbnail.jpg
navigation: True
title: The Silvias
date: 2022-12-31 14:30:13
tags: [art, portrait]
class: post-template
subclass: 'post'
author: derrick
excerpt: We went to the Franz Gertsch museum in Burgdorf today. His works came up because he died last week.
---
We went to the [Franz Gertsch museum](https://www.museum-franzgertsch.ch/en/) in Burgdorf today.
His works came up recently because he died last week.

His portraiture is simply awe inspiring.
The hyperrealism is at the level of [Albert Anker](https://en.wikipedia.org/wiki/Albert_Anker),
but the size is three meters on a side.
The lesson here is "go big or go home."

I bought the book "Silvia - Chronicle of a Painting" from the museum shop.
I don't really need more art books, but was intrigued by the techniques he uses.
He cheats like I do, with a projected photograph as a starting point.
Beyond that, he's - at not the next level but - dozens of levels beyond me.

Given a choice between [Paul Klee](https://en.wikipedia.org/wiki/Paul_Klee) and [Franz Gertsch](https://en.wikipedia.org/wiki/Franz_Gertsch),
I would choose Gertsch every time.
Klee wandered off into expressionism, bauhaus, and surrealism land and never came back.
I hope to be more in the realist camp, like Franz Gertsch and [Ken Danby](http://kendanbyart.ca/paintings#content).

All I need to do now, is source [#10 duck canvas](https://en.wikipedia.org/wiki/Cotton_duck) in three meter wide rolls and
inexpensive [lapis lazuli ultramarine blue pigment](https://www.kremer-pigmente.com/de/shop/pigmente/10530-lapis-lazuli-reinst.html).

| Data | Description |
| ----------- | ----------- |
| Title | Three "Silvia Paintings" |
| Artist | Franz Gertsch |
| Medium | Egg tempra on uncoated canvas |
| Dimensions (w &times; h cm) | 3 @ 280cm x 290cm |
| Date | 1998 - 2004 |

![three "Silvia" paintings by Franz Gertsch](/assets/images/silvias.jpg)