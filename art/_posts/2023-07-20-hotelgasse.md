---
layout: post
current: post
cover: assets/art/IMG_20230720_134854_thumbnail.jpg
navigation: True
title: Hotelgasse
date: 2023-07-20 11:48:54
tags: [art, retirement, stadtkünstler]
class: post-template
subclass: 'post'
author: derrick
excerpt: So I started my self-appointed Stadtkünstler (city artist) odyssey&#58; painting 'en plein air' in the Bern Altstadt, because no one else would want the job.
---
So I started my self-appointed Stadtkünstler (city artist) odyssey: painting ['en plein air'](https://en.wikipedia.org/wiki/En_plein_air) in the Bern Altstadt, because no one else would want the job.

I must admit up front that I cheat.
My ex-colleagues from [Ontinue](https://www.ontinue.com) bought me a portable beamer (German slang for a video projector) as a retirement gift
because I mentioned I was laying a piece of paper over my monitor to trace photographs - because I'm shit at perspective and freehand drawing.
With the beamer, I project a photograph onto a piece of paper and easily outline the image into a passable pencil sketch from which to start.
Then I either paint directly on the pencil sketch or apply waterproof ink with a fine-tipped old school nib pen.

For this painting, it's the latter. There's quite a lot of detail, and I try to retain the fidelity, but there are obvious mistakes in execution.
If the intent is to make a visual for someone who has never been there, watercolor has obviously been replaced by photography.
But, if the intent is to give someone a feel for the place based on the artists perception, then ... still not.
It would take me a while to add the skills necessary to pull that off.
And that's the challenge.



| Data | Description |
| ----------- | ----------- |
| Title | Hotelgasse |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 42 &times; 27.7 |
| Date | 2023 |

![watercolour of Hotelgasse in Bern Switzerland looking north](/assets/art/IMG_20230720_165737.jpg)

[original photograph of Hotelgasse](/assets/art/Hotelgasse.jpg)