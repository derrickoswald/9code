---
layout: post
current: post
cover: assets/art/IMG_20240430_203241_thumbnail.jpg
navigation: True
title: Gibson Girl
date: 2024-04-29 19:32:41
tags: [art, retirement, figure]
class: post-template
subclass: 'post'
author: derrick
comments_host: c.im
comments_user_id: derrickoswald
comments_id: 112361838021832747
excerpt: I like the Gibson Girl art style of Charles Dana Gibson, but thought it should be updated to the 21st century.
---

I like the [Gibson Girl art style of Charles Dana Gibson](https://en.wikipedia.org/wiki/Gibson_Girl), but thought it should be brought into the 21st century.
Modern women look somewhat different than those at the turn of the last century:
instead of corsets, ribbon and lace, it's backpacks, velcro and spandex.

I hope this is the first of many such paintings. I'm inspired by many women
&mdash; like my daughters &mdash;
that are self assured, confident, competent, bold and totally integrated into the
futuristic society we live in.
When I get good enough, I might even ask some of them to pose for me,
instead of serepticiously snapping a picture at a bus stop like this one.

It has been a while since I did an art, so of course I forgot everything I had learned.
In hindsight I should have done "wet in wet" for the black areas and
done more hatching for shadows. It is what it is.


| Data | Description |
| ----------- | ----------- |
| Title | Gibson Girl |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on cartridge paper |
| Dimensions (w &times; h cm)| 29.7 &times; 41 |
| Date | 2024 |

![watercolour and ink of a 21st century Gibson Girl](/assets/art/IMG_20240430_203241.jpg)

