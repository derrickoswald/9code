---
layout: post
current: post
cover: assets/art/IMG_20240416_140743_thumbnail.jpg
navigation: True
title: Casa Di Biase
date: 2024-04-16 12:07:43
tags: [art, retirement, civitella]
class: post-template
subclass: 'post'
author: derrick
excerpt: Once the village post office, this family house commands a stellar view of the Titerno valley and even comes with a built-in pizza oven.
---

Once the village post office, this family house commands a stellar view of the Titerno valley and even comes with a built-in pizza oven.

I struggle a lot with composition and placement,
wishing to bring the subject matter to the forefront,
but also wanting to include some of the milieu of the surrounding elements without detracting from the central core.
In this case I tried to place the scenic valley behind the house, but excluded the town surrounding it.
In hindsight, I should have backed out a bit and included more context in subdued shades.

| Data | Description |
| ----------- | ----------- |
| Title | Casa Di Biase |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on fine grain, acid free paper |
| Dimensions (w &times; h cm)| 29.7 &times; 21 |
| Date | 2024 |

![watercolour and ink of Franco's house](/assets/art/IMG_20240416_140743.jpg)
