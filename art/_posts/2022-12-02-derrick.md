---
layout: post
current: post
cover: assets/art/IMG_20221202_170139_thumbnail.jpg
navigation: True
title: Derrick
date: 2022-12-02 16:30:00
tags: [art, portrait]
class: post-template
subclass: 'post'
author: derrick
excerpt: Since no other colleagues volunteered to have their portrait made - having, I guess, been frightened off by my earlier works - I had to do myself.
---
Since no other colleagues volunteered to have their portrait made - having, I guess, been frightened off by my earlier works - I had to do myself.

I'd run out of the cartridge paper I was using and switched to an inexpensive Profi-Block paper, which is surprisingly hard to paint with.
Watercolour is absorbed almost immediately by this paper, allowing less time for feathering edges and dry-brush fix-ups.
It also provides less previous colour pick-up, when attempting wet-on-wet later on.
I'm sure I can get used to it, but my technique will need to change to accomodate the medium.

As for the subject; I'm sure I don't have that much hair and I failed to capture the Oswald mouth that Grandma gave us all.

| Data | Description |
| ----------- | ----------- |
| Title | Derrick |
| Artist | Derrick Oswald |
| Medium | Watercolour on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2022 |

![watercolor of Derrick](/assets/art/IMG_20221202_170139.jpg)

[original photograph of Derrick](/assets/art/Derrick.jpg)