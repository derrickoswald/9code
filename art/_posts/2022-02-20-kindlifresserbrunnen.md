---
layout: post
current: post
cover: assets/art/IMG_20221220_085157_thumbnail.jpg
navigation: True
title: Kindlifresserbrunnen
date: 2022-02-20 15:17:00
tags: [art, fountain]
class: post-template
subclass: 'post'
author: derrick
excerpt: Probably the most famous fountain in Bern, Kindlifresserbrunnen is a warning to children to behave or face dire consequences.
---
Probably the most famous fountain in Bern, Kindlifresserbrunnen is a warning to children to behave or face dire consequences.

From the street there is no really good angle to capture the actual child eating action, so I used a picture I found online as a basis.
It was [refurbished in 2015](https://www.bernerzeitung.ch/berner-brunnen-frisch-gewaschen-und-neu-lackiert-132893072734),
and that shot is likely taken just after that, with a long range lens from across the street to get several meters extra height on the angle.

I had thought to fill in the background buildings as well, but instead added a fake façade in outline in order to not detract from the focal point.

| Data | Description |
| ----------- | ----------- |
| Title | Kindlifresserbrunnen |
| Artist | Derrick Oswald |
| Medium | Watercolour and ink on cartridge paper |
| Dimensions (w &times; h cm)| 29.7 &times; 41 |
| Date | 20.02.2022 |

![watercolor of kindlifresserbrunnen](/assets/art/IMG_20221220_085157.jpg)