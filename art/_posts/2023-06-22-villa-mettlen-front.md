---
layout: post
current: post
cover: assets/art/IMG_20230622_163004_thumbnail.jpg
navigation: True
title: Villa Mettlen Take One
date: 2023-06-22 14:30:04
tags: [art, retirement, stadtkünstler]
class: post-template
subclass: 'post'
author: derrick
excerpt: I did a practice round of my 'city artist' fantasy by painting 'en plein air' just down the street. Here's what I learned.
---
I did a practice round of my 'city artist' fantasy by painting ['en plein air'](https://en.wikipedia.org/wiki/En_plein_air) just down the street. Here's what I learned.

- Glass jars are a no-go when working outdoors. I'm lucky I had one left to clean my brushes and provide water for mixing. I'm going to need some plastic cup-sized containers.

- 0.6 liters of water is not sufficient. The combination of water for mixing, cleaning and drinking in the hot sun exceeds the capacity of the little bottle I regularly carry. I'm going to need a one liter flask.

- Liquid paints are not really an option. It is better to have tubes of paste-type paint or solid dry pans in a suitable palette box. I can't believe how much liquid green can come out of a little 30ml bottle and make life so miserable.

- Paint dries a lot faster in the sun. If you think you're going to lay down a colour and go back to fix it, you're too late. You have to hit it correctly the first time - there is no backup plan.

- An easel is optional. I can hold the pad of paper with one hand and apply paint with the other. There were only a few times I needed to park the artwork while playing with accessories.

- It's a "one-big-brush" operation. There is no time and no need for a bunch of brushes smaller than a number ten. You just have to be able to handle a big brush well enough to do the detail work.

- It takes more than two hours to do a painting. My time budget was blown away by the time I had the basics painted in. I would now estimate at least three hours.

- There were people who stopped by and provided some comments. It is, as I expected, somewhat of a novelty for people to see an artist at work. I need an elevator pitch (in German) to explain how I will give them the painting - gratis - if they want it.


| Data | Description |
| ----------- | ----------- |
| Title | Villa Mettlen - front |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 42 &times; 29.7 |
| Date | 2023 |

![watercolour of the Villa Mettlen from the north](/assets/art/IMG_20230622_163004.jpg)

[original photograph of the Villa Mettlen](/assets/art/Villa%20Mettlen%20front.jpg)