---
layout: post
current: post
cover: assets/art/IMG_20221220_084929_thumbnail.jpg
navigation: True
title: Vennerbrunnen
date: 2019-05-29 00:00:00
tags: [art, fountain]
class: post-template
subclass: 'post'
author: derrick
excerpt: Of the many fountains in Bern, the Vennerbrunnen is one of my favourites, located in a courtyard in front of the old Rathaus.
---
Of the many fountains in Bern, the Vennerbrunnen is one of my favourites, located in a courtyard in front of the old Rathaus,
although it hasn't always been in the same [location](https://www.helveticarchives.ch/bild.aspx?VEID=801686&DEID=10&SQNZNR=1).
It turns out I, unknowingly, made a second painting on the same subject in 2022.

The decorated plinth and noble figure, with accompanying bear and flag, begs for an artistic treatment.
It's probably one of the most tourist captured images in and around Bern.

| Data | Description |
| ----------- | ----------- |
| Title | Vennerbrunnen |
| Artist | Derrick Oswald |
| Medium | Watercolour and ink on cartridge paper |
| Dimensions (w &times; h cm)| 29.7 &times; 41 |
| Date | 29.05.2020 |

![watercolor of vennerbrunnen](/assets/art/IMG_20221220_084929.jpg)