---
layout: post
current: post
cover: assets/art/IMG_20221220_085345_thumbnail.jpg
navigation: True
title: Vennerbrunnen
date: 2022-05-15 11:22:00
tags: [art, fountain]
class: post-template
subclass: 'post'
author: derrick
excerpt: This picture tries to capture the Rathausplatz in front of the old Rathaus with the Vennerbrunnen.
---
This picture tries to capture the Rathausplatz in front of the old Rathaus with the Vennerbrunnen,
one of the more famous fountains in Bern.

The astute observers from Bern will realize the statue is facing the wrong way,
however, the painting would not have the same flavour when looking at his butt.

I failed to capture the pale green sandstone of the Rathaus and St. Peter and Paul church,
because I'm still learning about watercolour mixing.

The roof could also use some hint of the tiles, but I'm lazy.

| Data | Description |
| ----------- | ----------- |
| Title | Vennerbrunnen |
| Artist | Derrick Oswald |
| Medium | Watercolour and ink on cartridge paper |
| Dimensions (w &times; h cm)| 41 &times; 29.7 |
| Date | 2022 |

![watercolor of vennerbrunnen](/assets/art/IMG_20221220_085345.jpg)