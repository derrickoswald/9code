---
layout: post
current: post
cover: assets/art/IMG_20221120_155550_thumbnail.jpg
navigation: True
title: Kristina
date: 2022-11-22 15:49:00
tags: [art, portrait]
class: post-template
subclass: 'post'
author: derrick
excerpt: For this portrait I tried not using an ink outline in order to soften up the edges.
---
For this portrait I tried not using an ink outline in order to soften up the edges, which sort of worked.
It's a lot more difficult to define edges when the colour and values are very similar between two surfaces.

I learned also that crossing lines are really not easy in watercolour, for example, hair vis-à-vis sweater ribbing.
Sunlight coming through hair is also very difficult to portray, needing infinitely thin lines in general.

| Data | Description |
| ----------- | ----------- |
| Title | Kristina |
| Artist | Derrick Oswald |
| Medium | Watercolour on cartridge paper |
| Dimensions (w &times; h cm)| 29.7 &times; 41 |
| Date | 2022 |

![watercolor of Kristina](/assets/art/IMG_20221120_155550.jpg)

[original photograph of Kristina](/assets/art/Kristina.jpg)