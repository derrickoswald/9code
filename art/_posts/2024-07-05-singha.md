---
layout: post
current: post
cover: assets/art/IMG_20240705_053752_thumbnail.jpg
navigation: True
title: Singha
date: 2024-07-05 17:37:52
tags: [art, portrait]
class: post-template
subclass: 'post'
author: derrick
comments_host: c.im
comments_user_id: derrickoswald
comments_id: 112767112568625070
excerpt: Part of the experience of going to Alesse is interacting with an old dog named Singha.
---
Part of the experience of going to Alesse is interacting with an old dog named Singha. I'm told the name is spelled like the [beer](https://singha.com/).
Inevitably, when you open the front door there is a bark or two and a mottled bundle of activity will start sucking up to you like you're the last person on earth.

I've wanted to do a pastel pencil portrait of Singha for a while, but I've never gotten around to it.
Instead I thought I would experiment with using a brush and my daily driver ink (the one I use in my fountain pen) to see what that's like.
I found out that ink is not as black as you might think, and it's very wet - so it soaks through the paper.

Since I don't have any ink colours other than black, I had to mix in some watercolour brown and grey to get something similar to the actual visual.
This is mostly a throw-away piece, being only an experiment.
Critics tell me, though, I could have done better with the fur and be more delicate with the linework - as I am wont do.
To be honest though, I was trying to make it easier to generate these pieces,
because low effort paintings like this you can get in any modern art museum for 'ein paar hundert Stutz' (a few hundred dollars),
and I wanted to check out the economics of making art for profit.
So far, I don't see any compelling lucre, so I'm still a chill artisan.

| Data | Description |
| ----------- | ----------- |
| Title | Singha |
| Artist | Derrick Oswald |
| Medium | Ink and watercolour on 100g/m<sup>2</sup> plain paper |
| Dimensions (w &times; h cm)| 29.7 &times; 42 |
| Date | 2024 |

![ink and watercolour of Singha](/assets/art/IMG_20240705_053752.jpg)

[original photograph of Singha](/assets/art/Singha.jpg)