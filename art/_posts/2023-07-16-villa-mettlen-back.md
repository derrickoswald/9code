---
layout: post
current: post
cover: assets/art/IMG_20230716_195801_thumbnail.jpg
navigation: True
title: Villa Mettlen Take Two
date: 2023-07-16 17:58:01
tags: [art, retirement, stadtkünstler]
class: post-template
subclass: 'post'
author: derrick
excerpt: I did another practice round of my 'city artist' fantasy by painting 'en plein air', again, just down the street.
---
I did another practice round of my 'city artist' fantasy by painting ['en plein air'](https://en.wikipedia.org/wiki/En_plein_air), again, just down the street. Here's what I learned his time.

- I still need to carry liquid paint, just for my opaque white, which is sort of a guache

- stand in the shade when taking the reference picture, i.e. where you're going to position yourself when you come back to paint, because painting in the direct sun is not much fun

- the reference picture should not be from too long ago; the Wisteria was no longer in bloom and I had to fake it

- I should probably use two palettes, because I ran out of paint wells and there's no way to clean it in the field

- I should probably bring a plastic bag and paper towels to wrap the dirty palette(s) in to take home in my backpack, a tissue tends to stick and doesn't cover it completely

- the little three legged stool is just barely comfortable enough &mdash; it requires many standing and stretching breaks

- I can get by with a 0.6 liter water bottle if there is a fountain nearby

- three hours is just sufficient to do a sort of passable job; plan on more time if possible

- the lid of my broken Winsor and Newton paint box should not be used as a palette since it falls off due to the broken hinge

- the subtle green of Ostermundigen sandstone eluded me today with the Lemon Yellow + Cerulean Blue + Quinacridone Violet combination, so it's either more subtle than I thought or I should go back to Cadmium Yellow + Ultramarine Blue + Burnt Umber


| Data | Description |
| ----------- | ----------- |
| Title | Villa Mettlen - back |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 42 &times; 29.7 |
| Date | 2023 |

![watercolour of the Villa Mettlen from the south](/assets/art/IMG_20230716_195801.jpg)

[original photograph of the Villa Mettlen](/assets/art/Villa%20Mettlen%20back.jpg)