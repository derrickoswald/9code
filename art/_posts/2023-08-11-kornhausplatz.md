---
layout: post
current: post
cover: assets/art/IMG_20230815_110923_thumbnail.jpg
navigation: True
title: Kornhausplatz
date: 2023-08-15 09:09:23
tags: [art, retirement, stadtkünstler]
class: post-template
subclass: 'post'
author: derrick
excerpt: I wanted to loosen up on the detail a bit this time, which means drawing and painting in the style of an artist instead of a draftsman. A sad fail of course.
---
I wanted to loosen up on the detail a bit this time, which means drawing and painting in the style of an artist instead of a draftsman. A sad fail of course.

The idea was to paint a streetcar *in loco* like [José Melgratti](https://pixelfed.social/JoseMel)
but with a few other effects like leaving areas unpainted, wet-in-wet pavement, colouring outside the lines,
and adding spatter and blotches.

The initial ink sketch also needs to be more freeform; which causes me angst because I'm the type that grunts audibly when I draw an ink line out of place.
So this is an attempt to draw fewer lines and leave more to the viewer's imagination, inking only in the shadow areas just to add tone.

I don't know the workflow of other artists, but here's mine.

- take a photograph with suitable composition, colour and interest
- study the photograph in detail on my big monitor at home to form a plan
- the plan takes the form of a hand drawn A4 pencil sketch on watercolour paper with many annotations
- project the photograph on the target A3 paper and draw landmarks and details in pencil to make a draft image
- edit the pencil draft to smooth lines for perspective and add/remove/improve features by eye
- ink the sketch with waterproof ink, using a fine nib, for areas that will retain an ink outline in the final art
- erase the pencil sketch where the ink supercedes, but leaving the pencil for free-form areas
- apply watercolour, lightest - area wash - colours first, followed by more and more detailed colours and shadows
- use the A4 plan as a colour worksheet to test mixes and densities
- play around with adding details and "improving it" until you finally give up and say "it's OK as is, ship it"
- title, sign and date the painting


| Data | Description |
| ----------- | ----------- |
| Title | Kornhausplatz |
| Artist | Derrick Oswald | 
| Medium | Watercolour and ink on acid-free, cellulose, fine-grained, natural-white paper |
| Dimensions (w &times; h cm)| 42 &times; 29.7 |
| Date | 2023 |

![watercolour of the Kornhausplatz in Bern Switzerland featuring the bright red number 9 tram](/assets/art/IMG_20230815_110923.jpg)

[original photograph of Kornhausplatz](/assets/art/Kornhausplatz.jpg)