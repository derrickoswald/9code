# 9code

This is a static website generator for 9code. It is based on the [Jasper2](https://github.com/jekyllt/jasper2) theme and [Jekyll](https://jekyllrb.com/).

## Setup

### Install Ruby

    sudo apt-get install ruby
    sudo apt-get install ruby-dev
    bundle config set --local path 'vendor/bundle'
    bundle install

### Fix Ruby

On Ubuntu, the default Ruby is 3.0.2p107 (2021-07-07 revision 0db68f0233) [x86_64-linux-gnu], which means Jekyll is broken (`bundle exec jekyll serve` throws `Error: no implicit conversion of Hash into Integer`).

To fix this edit (as root) the pathutil.rb file:

`vi vendor/bundle/ruby/3.0.0/gems/pathutil-0.16.2/lib/pathutil.rb`

and change `kwd` to `**kwd` in 10 places:

```
        497c497
        <       File.read(self, *args, kwd).encode({
        ---
        >       File.read(self, *args, **kwd).encode({
        503c503
        <         self, *args, kwd
        ---
        >         self, *args, **kwd
        517c517
        <       File.binread(self, *args, kwd).encode({
        ---
        >       File.binread(self, *args, **kwd).encode({
        523c523
        <         self, *args, kwd
        ---
        >         self, *args, **kwd
        537c537
        <       File.readlines(self, *args, kwd).encode({
        ---
        >       File.readlines(self, *args, **kwd).encode({
        543c543
        <         self, *args, kwd
        ---
        >         self, *args, **kwd
        559c559
        <       ), *args, kwd)
        ---
        >       ), *args, **kwd)
        563c563
        <         self, data, *args, kwd
        ---
        >         self, data, *args, **kwd
        579c579
        <       ), *args, kwd)
        ---
        >       ), *args, **kwd)
        583c583
        <         self, data, *args, kwd
        ---
        >         self, data, *args, **kwd
```
### Fix Jasper2

With ruby 3.0 a number of gems are no longer included, specifically `webrick`.
In the Jasper2 directory, add webrick with the command:

```
bundle add webrick
```

This has been done for the 9code site (Gemfile in this repo), and need not be done again.

### Fix Configuration

There is a lot to change, but to start, change the `baseurl` and `destination` settings in `_config.yml`:

```
baseurl: /
destination: ./_site
```

### Install gulp

To get css file changes to be used, you need to run [gulp](https://gulpjs.com/), which, like every NodeJS package I've ever encountered is a shitshow of broken dependencies.

I am not an expert, so I struggled through by upgrading to node v18.12.1 (I use nvm) and then sort of following the instructions to install [gulp](https://gulpjs.com/docs/en/getting-started/quick-start/) and [Googling](https://stackoverflow.com/questions/55921442/how-to-fix-referenceerror-primordials-is-not-defined-in-node-js) how to fix a dependency issue with an override to graceful-js - which is obviously *not* graceful.

### Tweak Apache

The lazy Jekyll framework only uses partial URL values, that is, a page on disk named `/var/www/html/post1.html` is only linked with `/post1`, which is missing the `.html` extension.

Rather than try to figure out how to fix the Ruby code, configure Apache to use [MultiViews](https://httpd.apache.org/docs/2.4/content-negotiation.html)
by adding this `Directory` fragment to every `VirtualHost` in `/etc/apache2/sites-available/000-default.conf`:

```
        <Directory /var/www/html>
                Options +MultiViews
        </Directory>
```

## Linking to Mastodon for Comments

See _includes/fediverse_comments.html, and add this into post header:

    comments_host: c.im
    comments_user_id: derrickoswald
    comments_id: 55555

## Deployment

The procedure hasn't been worked out completely, but it should be something like:
```
rm -rf _site
nvm use lts/hydrogen
./node_modules/.bin/gulp css
bundle exec jekyll build
rsync --verbose --recursive --links --perms --times --update --compress _site pi@cyclone:/home/pi
ssh pi@cyclone
sudo rsync --verbose --recursive --links --perms --times --update /home/pi/_site/* /var/www/html
exit
```
