---
layout: post
current: post
cover: assets/letters/20230525_thumbnail.jpg
navigation: True
title: Lifestyles
date: 2023-05-25 10:55:12
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, I've been thinking about the lifestyle differences between Canada and Switzerland lately."
---
![cursive letter about lifestyles](/assets/letters/20230525.jpg#full)

