---
layout: post
current: post
cover: assets/letters/20230319_thumbnail.jpg
navigation: True
title: Financial Investment
date: 2023-03-19 10:40:12
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, As I mentioned on the phone, I've picked up a new hobby, one you're familiar with: financial investment"
---
![cursive letter about financial investment](/assets/letters/20230319.jpg#full)
