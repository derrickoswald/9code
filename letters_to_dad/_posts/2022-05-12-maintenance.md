---
layout: post
current: post
cover: assets/letters/20220512_thumbnail.jpg
navigation: True
title: Maintenance
date: 2022-05-15 19∶11∶01
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Mom and Dad, You probably don't think about it much anymore, but there was a time when house maintennance was a big part of your lives.
---
![cursive letter about maintenance](/assets/letters/20220512.jpg#full)