---
layout: post
current: post
cover: assets/letters/20241013_thumbnail.jpg
navigation: True
title: Gambling
date: 2024-10-13 14:00:10
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear dad, I've never been much of a gambler. I attribute this to an experience early on in Calgary"
---
![cursive letter about gambling](/assets/letters/20241013.jpg#full)
