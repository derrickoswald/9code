---
layout: post
current: post
cover: assets/letters/20221222_thumbnail.jpg
navigation: True
title: Swimming
date: 2022-12-22 15∶12∶24
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, When we first moved in to Misener Crescent, we explored a lot around the Sheridan Park research center."
---
![cursive letter about swimming](/assets/letters/20221222.jpg#full)
