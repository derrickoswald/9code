---
layout: post
current: post
cover: assets/letters/20231030_thumbnail.jpg
navigation: True
title: Urban Sketching
date: 2023-10-30 06:58:50
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, We all have 10,000 bad drawings in us. The sooner we get them out the better -- Walt Stanchfield"
---
![cursive letter about urban sketching](/assets/letters/20231030.jpg#full)
