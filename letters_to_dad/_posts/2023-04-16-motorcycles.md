---
layout: post
current: post
cover: assets/letters/20230416_thumbnail.jpg
navigation: True
title: Motorcycles
date: 2023-04-16 11:30:08
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, At this point in the spring season, every man's heart turns towards convertibles and motorcycles."
---
![cursive letter about motorcycles](/assets/letters/20230416_1.jpg#full)
![watercolor of cutlery](/assets/letters/20230416_2.jpg#full)
