---
layout: post
current: post
cover: assets/letters/20221106_thumbnail.jpg
navigation: True
title: Halloween
date: 2022-11-06 20∶57∶03
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, They don't celebrate Haloween here like we used to as kids. Very few houses do it, and none of them are decorated.
---
![cursive letter about halloween](/assets/letters/20221106.jpg#full)
