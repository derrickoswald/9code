---
layout: post
current: post
cover: assets/letters/20250106_thumbnail.jpg
navigation: True
title: Road Hockey
date: 2025-01-06 15:35:30
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear dad, Nobody in our childhood peer group played ice-hockey - the equipment was too expensive - but we did play a lot of road-hockey."
---
![cursive letter about road hockey](/assets/letters/20250106.jpg#full)
