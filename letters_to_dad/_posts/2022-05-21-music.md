---
layout: post
current: post
cover: assets/letters/20220521_thumbnail.jpg
navigation: True
title: Music
date: 2022-05-28 10∶38∶32
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Mom and Dad, Remember the dark-wood, upright piano in the family-room we took lessons on?
---
![cursive letter about music](/assets/letters/20220521.jpg#full)
