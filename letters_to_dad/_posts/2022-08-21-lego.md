---
layout: post
current: post
cover: assets/letters/20220821_thumbnail.jpg
navigation: True
title: Lego
date: 2022-08-21 10∶12∶43
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, We always seemed to have enough toys to play with. Hot Wheels, Slinkys, cap guns and such were always around, although not always in top condition.
---
![cursive letter about lego](/assets/letters/20220821.jpg#full)
