---
layout: post
current: post
cover: assets/letters/20240725_thumbnail.jpg
navigation: True
title: Bricolage
date: 2024-07-25 15:53:27
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear dad, I'm constantly striving towards the goal you instilled in us as kids to do 'bricolage' - the French word best translated as do-it-yourself (DIY)."
---
![cursive letter about the Cadillac](/assets/letters/20240725.jpg#full)
