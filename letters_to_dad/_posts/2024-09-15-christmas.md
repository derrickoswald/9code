---
layout: post
current: post
cover: assets/letters/20240915_thumbnail.jpg
navigation: True
title: Christmas
date: 2024-09-15 15:51:44
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear dad, We're making plans to visit Canada for Christmas. I assume there is still turkey eaten and carols sung..."
---
![cursive letter about Christmas](/assets/letters/20240915.jpg#full)
