---
layout: post
current: post
cover: assets/letters/20220711_thumbnail.jpg
navigation: True
title: Homestead
date: 2022-07-11 19∶08∶00
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, Everyone, except me, lives within five or ten kilometers of 2611 Misener Cresecnt - the old homestead.
---
![cursive letter about the homestead](/assets/letters/20220711.jpg#full)
