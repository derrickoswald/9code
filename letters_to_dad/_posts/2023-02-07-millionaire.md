---
layout: post
current: post
cover: assets/letters/20230207_thumbnail.jpg
navigation: True
title: Millionaire
date: 2023-02-07 08:04:03
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, growing up I'd often say my goal was to be a millionaire."
---
![cursive letter about being a millionaire](/assets/letters/20230207.jpg#full)
