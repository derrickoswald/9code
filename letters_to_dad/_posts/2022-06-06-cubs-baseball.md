---
layout: post
current: post
cover: assets/letters/20220606_thumbnail.jpg
navigation: True
title: Cubs and Baseball
date: 2022-06-06 09∶42∶55
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, Today I turn towards organized activities that we used to do, like cubs and baseball.
---
![cursive letter about cubs and baseball](/assets/letters/20220606.jpg#full)
