---
layout: post
current: post
cover: assets/letters/20230924_thumbnail.jpg
navigation: True
title: Self Discipline
date: 2023-09-24 11:50:39
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, I quit smoking. Again. It's just one of many of my bad habits that require self discipline to kick."
---
![cursive letter about self discipline](/assets/letters/20230924.jpg#full)
