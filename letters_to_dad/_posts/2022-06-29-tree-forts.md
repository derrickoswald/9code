---
layout: post
current: post
cover: assets/letters/20220629_thumbnail.jpg
navigation: True
title: Tree Forts
date: 2022-06-29 18∶58∶33
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, Up on Thornlodge, just past Sheridan Park Public School was Loyalist Creek. It was rather shallow and ran in a man-made bare rock channel behind the school.
---
![cursive letter about tree forts](/assets/letters/20220629.jpg#full)
