---
layout: post
current: post
cover: assets/letters/20220612_thumbnail.jpg
navigation: True
title: Construction
date: 2022-06-12 14∶59∶15
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, We did a lot of construction as kids, from soap-box go-karts to tree forts.
---
![cursive letter about construction](/assets/letters/20220612.jpg#full)
