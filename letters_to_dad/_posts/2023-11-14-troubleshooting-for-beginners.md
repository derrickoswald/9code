---
layout: post
current: post
cover: assets/letters/20231114_thumbnail.jpg
navigation: True
title: Troubleshooting for Beginners
date: 2023-11-14 10:57:32
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, I'm writing a book: 'Troubleshooting for Beginners' in ePub format."
---
![cursive letter about the book Troubleshooting for Beginners](/assets/letters/20231114.jpg#full)
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;[Troubleshooting.epub](https://gitlab.com/derrickoswald/troubleshooting/-/blob/master/Troubleshooting.epub?ref_type=heads)