---
layout: post
current: post
cover: assets/letters/20220904_thumbnail.jpg
navigation: True
title: Machine Learning
date: 2022-09-04 11∶52∶55
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, On or about 1983 I was tasked with organizing the get-together discussion for Christmas. The topic I chose was machine intelligence.
---
![cursive letter about machine learning](/assets/letters/20220904_1.jpg#full)
![cursive letter about machine learning](/assets/letters/20220904_2.jpg#full)
