---
layout: post
current: post
cover: assets/letters/20221030_thumbnail.jpg
navigation: True
title: Art
date: 2022-10-30 20∶40∶53
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Dad, Last time on the phone, we discused the art you've done - the nails and string piece and the living-room Paris mural. That got me thinking a bit.
---
![cursive letter about art](/assets/letters/20221030.jpg#full)
