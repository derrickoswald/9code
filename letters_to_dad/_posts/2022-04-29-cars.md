---
layout: post
current: post
cover: assets/letters/20220429_thumbnail.jpg
navigation: True
title: Cars
date: 2022-04-29 09∶28∶15
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: Dear Mom and Dad, I would certainly be remiss if I didn't mention the cars of my childhood. The first car I remember was the Chevrolet Biscayne with the tail fins
---
![cursive letter about cars](/assets/letters/20220429.jpg#full)