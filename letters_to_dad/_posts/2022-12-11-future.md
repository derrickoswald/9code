---
layout: post
current: post
cover: assets/letters/20221211_thumbnail.jpg
navigation: True
title: The Future
date: 2022-12-11 18∶36∶08
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, I've got a favorite saying: \"I love living in the future\". Which is to say: I'm always delighted with how modern things ease daily life."
---
![cursive letter about the future](/assets/letters/20221211_1.jpg#full)
![cursive letter about the future](/assets/letters/20221211_2.jpg#full)
