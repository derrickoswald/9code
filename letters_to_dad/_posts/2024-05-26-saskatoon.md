---
layout: post
current: post
cover: assets/letters/20240526_thumbnail.jpg
navigation: True
title: Saskatoon
date: 2024-05-26 13:01:46
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, By several serendipitous coincidences we met up with another person who was born in Saskatoon."
---
![cursive letter about Saskatoon](/assets/letters/20240526.jpg#full)
