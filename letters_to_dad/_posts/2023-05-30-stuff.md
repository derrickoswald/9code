---
layout: post
current: post
cover: assets/letters/20230530_thumbnail.jpg
navigation: True
title: Stuff
date: 2023-05-30 19:41:02
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, One of the items on my extensive ToDo list is to cull my stuff."
---
![cursive letter about stuff page 1](/assets/letters/20230530_1.jpg#full)
![cursive letter about stuff page 2](/assets/letters/20230530_2.jpg#full)

