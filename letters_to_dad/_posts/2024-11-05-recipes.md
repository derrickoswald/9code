---
layout: post
current: post
cover: assets/letters/20241105_thumbnail.jpg
navigation: True
title: Recipes
date: 2024-11-05 13:47:48
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear dad, I made Guacamole yesterday. I used to have only a few recipes in my quiver..."
---
![cursive letter about recipes](/assets/letters/20241105.jpg#full)
