---
layout: post
current: post
cover: assets/letters/20230111_thumbnail.jpg
navigation: True
title: Retirement
date: 2023-01-11 08∶19∶11
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, I've been surprisingly busy, considering I'm retired now. It's not like summer vacation..."
---
![cursive letter about retirement](/assets/letters/20230111.jpg#full)
