---
layout: post
current: post
cover: assets/letters/20240714_thumbnail.jpg
navigation: True
title: Cadillac
date: 2024-07-14 15:27:33
tag: letters
class: post-template
subclass: 'post'
author: derrick
excerpt: "Dear Dad, One car that I failed to mention in my previous two letters about cars, was the Cadillac."
---
![cursive letter about the Cadillac](/assets/letters/20240714.jpg#full)
